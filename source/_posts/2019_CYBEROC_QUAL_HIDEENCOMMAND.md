---
title: 2019 사이버 작전 경연 대회 예선 Hidden Command (2019 CYBEROC QUAL)
---

# Hidden Command

```python
def clear_user(user):
    user.username = admin_username
    user.password = admin_password
    user.email = admin_email
```

위 소스에서 ``clear_user()`` 함수가 호출이 될 경우, ``비밀번호 재설정``이 된 계정이 ``Admin`` 계정이 되도록 되어 있다.
그러므로, 이를 어디서 호출하는지 알아본 뒤 익스해보도록 하자!

```python
def forget():
    form = ForgetForm(request.form)
    msg = None
    if request.method == "POST" and form.validate():
        msg = "User not found"
        user = User.query.filter_by(username = form.username.data, email = form.email.data).first()
        if user:
            new_password = rand_password(10)
            new_user = User(form.username.data, new_password, form.email.data)
            clear_user(user)
            db_session.add(new_user)

            board_lst = Board.query.filter_by(username = form.username.data).all()
            for b in board_lst:
                b.username = admin_username

            db_session.commit()
            return render_template("forget_res.html", password = new_password)
    return render_template("forget.html", msg = msg, form = form)
```

``forget 함수``에서 ``clear_user()``로 자연스럽게 호출하며, 해당 아이디는 ``admin``으로 되게 된다.
비밀번호 변경 이후에는 기존 세션은 날리는 것이 정상이고, 해당 세션 아이디가 ``admin``가 되는 현상이 발생하는데.
이를 이용해서 ``admin`` 계정이 되었으니, 플래그가 담긴 게시물을 볼 수 있게 된다.

첫번째 게시물을 보면 ``Flag``가 나오는 것을 볼 수 있었다.

### Flag is ``FLAG{N0t_s3cur3_4t_411}``
